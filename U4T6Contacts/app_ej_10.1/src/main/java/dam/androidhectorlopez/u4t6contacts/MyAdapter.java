package dam.androidhectorlopez.u4t6contacts;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private MyContacts myContacts;
    private ArrayList<ContactItem> cItem;

    // Class for each item: contains only a TextView
    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        private ImageView iv_Photo;
        private TextView tv_id;
        private TextView tv_name;
        private TextView tv_phone;


//        public MyViewHolder(TextView view) {
//            super(view);
//            this.textView = view;
//            tv_id = view.findViewById(R.id.tv_Id);
//            tv_name = view.findViewById(R.id.tv_Name);
//            tv_phone = view.findViewById(R.id.tv_NumberPhone);
//        }

        // TODO: 19/12/2019 Aci es on assignem la id a cada element del RecyclerView 
        public MyViewHolder(View view) {
            super(view);
            //this.textView = view;
            tv_id = view.findViewById(R.id.tv_Id);
            tv_name = view.findViewById(R.id.tv_Name);
            tv_phone = view.findViewById(R.id.tv_NumberPhone);
            iv_Photo = view.findViewById(R.id.iv_Photo);
        }

    }

    // TODO: 19/12/2019 Aquest es el constructor del Adapter on li passem per parametre els arraylist de contactes 
    // constructor: myContacts contains Contacts data
    MyAdapter(MyContacts myContacts, ArrayList<ContactItem> cItem) {
        this.myContacts = myContacts;
        this.cItem = cItem;
    }


    // TODO: 19/12/2019 Aci inflem la vista del Layout 
    // Create new view item: Layout Manager calls this method
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create item view:
        // use a simple textview predefined layout(skd/platforms/android-xx/data/res/layout) that conatins only TextView
//        TextView tv = (TextView) LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_view, parent, false);

//        return new MyViewHolder(tv);
        return new MyViewHolder(view);
    }

    // TODO: 19/12/2019 En aquesta funcio afegim el contingut a cada element del recyclerView 
    //replaces the data content of a viewholder (recycles old viewholder): Layout manager calls this method
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.tv_phone.setText(cItem.get(position).getNumberPhone());
        viewHolder.tv_name.setText(cItem.get(position).getName());
        viewHolder.tv_id.setText(cItem.get(position).getId() + "");

        if (cItem.get(position).getPhotoUrl() != "none") {
            viewHolder.iv_Photo.setImageURI(Uri.parse(cItem.get(position).getPhotoUrl()));
        }


    }

    // returns the size of dataSet: Layout Manager calls this method
    @Override
    public int getItemCount() {
        return cItem.size();
    }
}
