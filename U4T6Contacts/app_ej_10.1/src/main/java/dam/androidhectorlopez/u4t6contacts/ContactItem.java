package dam.androidhectorlopez.u4t6contacts;

public class ContactItem {
    private int id;
    private String numberPhone;
    private String name;
    private String type;
    private String photoUrl;

    // TODO: 19/12/2019 Aquesta es la classe dels contactes telefonics
    public ContactItem(int id, String numberPhone, String name, String type, String photoUrl) {
        this.id = id;
        this.numberPhone = numberPhone;
        this.name = name;
        this.type = type;
        this.photoUrl = photoUrl;
    }

    public ContactItem() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
