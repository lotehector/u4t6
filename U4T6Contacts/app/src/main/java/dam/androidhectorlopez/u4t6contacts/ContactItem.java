package dam.androidhectorlopez.u4t6contacts;

public class ContactItem {
    private int id;
    private String numberPhone;
    private String name;
    private String type;
    private String photoUrl;
    private String lookUpKey;
    private String uri;

    // TODO: 19/12/2019 Aquesta es la classe dels contactes telefonics
    public ContactItem(int id, String numberPhone, String name, String type, String photoUrl, String lookUpKey) {
        this.id = id;
        this.numberPhone = numberPhone;
        this.name = name;
        this.type = type;
        this.photoUrl = photoUrl;
        this.lookUpKey = lookUpKey;
        this.uri = uri;
    }

    public ContactItem() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getLookUpKey() {
        return lookUpKey;
    }

    public void setLookUpKey(String lookUpKey) {
        this.lookUpKey = lookUpKey;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public String toString() {
        return "ContactItem{" +
                "id=" + id +
                ", numberPhone='" + numberPhone + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                ", lookUpKey='" + lookUpKey + '\'' +
                '}';
    }
}
