package dam.androidhectorlopez.u4t6contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import java.util.ArrayList;

public class MyContacts {
    private ArrayList<String> myDataSet;
    private Context context;
    private ArrayList<ContactItem> contactItems = new ArrayList<>();

    public MyContacts(Context context) {
        this.context = context;
        this.myDataSet = getContacts();
    }

    // Get contacts list from ContactsProviders
    private ArrayList<String> getContacts() {
        ArrayList<String> contactList = new ArrayList<>();

        // Access to contentProviders
        ContentResolver contentResolver = context.getContentResolver();

        // TODO: 19/12/2019 Aquest array es on estan les sentencies de la query que volem buscar
        // Aux variables
        String[] projection = new String[]{ContactsContract.Data._ID, ContactsContract.Data.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.Data.CONTACT_ID, ContactsContract.Data.LOOKUP_KEY, ContactsContract.Data.RAW_CONTACT_ID, ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.Data.PHOTO_THUMBNAIL_URI};

        // TODO: 19/12/2019 Aco es un filtre per a que ens mostreels numeros de telefon que no siguen null
        String selectionFilter = ContactsContract.Data.MIMETYPE + "='" + ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE + "' AND " + ContactsContract.CommonDataKinds.Phone.NUMBER + " IS NOT NULL";

        //Query required data
        Cursor contactsCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI, projection, selectionFilter, null, ContactsContract.Data.DISPLAY_NAME + " ASC");

        // TODO: 19/12/2019 En aquesta condicio rebem el numero de columna on esta cada dada aixi podem encontrar-la despres
        if (contactsCursor != null) {
            //get the column indexes for desired Name and Number columns
            int nameIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME);
            int numberIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int numberContactID = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.CONTACT_ID);
            int numberRaw = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.RAW_CONTACT_ID);
            int numType = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.TYPE);
            int numLookUP = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.LOOKUP_KEY);
            int numPhoto = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.PHOTO_THUMBNAIL_URI);
//            int uri = contactsCursor.getColumnIndexOrThrow(String.valueOf(ContactsContract.Data.CONTENT_URI));


            //read data and add to ArrayList
            while (contactsCursor.moveToNext()) {
                int id = contactsCursor.getInt(numberContactID);
                String name = contactsCursor.getString(nameIndex);
                String number = contactsCursor.getString(numberIndex);
                int type = contactsCursor.getInt(numType);
                String raw = contactsCursor.getString(numberRaw);
                String lookUp = contactsCursor.getString(numLookUP);
                String photo = contactsCursor.getString(numPhoto);
                String tipus;
//                String ur = contactsCursor.getString(uri);

                // TODO: 19/12/2019 Aci en fiquem el tipus de telefon que siga 
                switch (type) {
                    case 1:
                        tipus = "Work";
                        break;
                    case 2:
                        tipus = "Mobile";
                        break;
                    case 3:
                        tipus = "Home";
                        break;
                    default:
                        tipus = "None";
                }
                if (photo == null) {
                    photo = "none";
                }

                // TODO: 19/12/2019 Aci creem un nou contactItem amb els les dades que hem rebut i ho enmagatzenem en un arrayLists 
                contactList.add(id + " : " + name + ": " + number + " type: " + tipus + " raw " + raw + " LookUp " + lookUp);
                contactItems.add(new ContactItem(id, number, name, tipus, photo, lookUp));

            }
            contactsCursor.close();
        }
        return contactList;
    }

    public String getContactData(int position) {
        return myDataSet.get(position);
    }

    public ArrayList<ContactItem> getContactes() {
        return contactItems;
    }

    public int getCount() {
        return myDataSet.size();
    }

    public void viewContact(Uri contactUri) {
        Intent intent = new Intent(Intent.ACTION_VIEW, contactUri);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }
}
